
CREATE TABLE `foodOrder`.`categoryDetails` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `categoryName` VARCHAR(255) NOT NULL DEFAULT '',
  `desc` VARCHAR(45) NULL DEFAULT '',
  `displayIndex` INT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
  );
  
INSERT INTO `foodOrder`.`categoryDetails` (`id`, `categoryName`, `desc`, `displayIndex`) VALUES ('1', 'Starter', 'first Item', '1');
INSERT INTO `foodOrder`.`categoryDetails` (`id`, `categoryName`, `desc`, `displayIndex`) VALUES ('2', 'Main Course', 'secound Item', '2');
  

CREATE TABLE `foodOrder`.`subCategoryDetails` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `subCategoryName` VARCHAR(255) NULL DEFAULT '',
  `desc` VARCHAR(255) NULL DEFAULT '',
  PRIMARY KEY (`id`));

INSERT INTO `foodOrder`.`subCategoryDetails` (`id`, `subCategoryName`) VALUES ('1', 'Chinees');
INSERT INTO `foodOrder`.`subCategoryDetails` (`id`, `subCategoryName`) VALUES ('2', 'Indian');
INSERT INTO `foodOrder`.`subCategoryDetails` (`id`, `subCategoryName`) VALUES ('3', 'Maxican');

  
CREATE TABLE `foodOrder`.`OrderMenuDetails` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `itemName` VARCHAR(4255) NOT NULL DEFAULT '',
  `desc` VARCHAR(45) NULL DEFAULT '',
  `categoryId` INT NOT NULL,
  `subCategoryId` INT NOT NULL,
  `price` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `categoryId_idx` (`categoryId` ASC),
  INDEX `subCategoryId_idx` (`subCategoryId` ASC),
  CONSTRAINT `categoryId`
    FOREIGN KEY (`categoryId`)
    REFERENCES `foodOrder`.`categoryDetails` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `subCategoryId`
    FOREIGN KEY (`subCategoryId`)
    REFERENCES `foodOrder`.`subCategoryDetails` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
    
    
    
CREATE TABLE `foodOrder`.`userDetails` (
  `userId` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL DEFAULT '',
  `email` VARCHAR(60) NOT NULL DEFAULT '',
  `pwd` VARCHAR(45) NOT NULL DEFAULT '',
  `phone` VARCHAR(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`userId`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  UNIQUE INDEX `phone_UNIQUE` (`phone` ASC));
  
 CREATE TABLE `foodOrder`.`OrderUserMapping` (
  `OrderId` INT NOT NULL,
  `userId` INT NOT NULL DEFAULT 0,
  `createdDate` DATETIME NULL,
  INDEX `userId_FK_idx` (`userId` ASC),
  CONSTRAINT `userId_FK`
    FOREIGN KEY (`userId`)
    REFERENCES `foodOrder`.`userDetails` (`userId`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
    
    
 CREATE TABLE `foodOrder`.`orderDetails` (
  `OrderId` INT NOT NULL DEFAULT 0,
  `orderMenuId` INT NOT NULL DEFAULT 0,
  `specialReq` VARCHAR(255) NULL,
  `quantity` INT NOT NULL DEFAULT 0,
  `price` INT NOT NULL DEFAULT 0,
  INDEX `orderMenuId_FK_idx` (`orderMenuId` ASC),
  CONSTRAINT `orderMenuId_FK`
    FOREIGN KEY (`orderMenuId`)
    REFERENCES `foodOrder`.`OrderMenuDetails` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
    
    
 CREATE TABLE `foodOrder`.`cart` (
  `cartId` INT NOT NULL AUTO_INCREMENT,
  `totalValue` INT NOT NULL DEFAULT 0,
  `description` VARCHAR(255) NULL,
  `isForDelivery` TINYINT NOT NULL DEFAULT 0,
  `isCompleted` TINYINT NULL DEFAULT 0,
  PRIMARY KEY (`cartId`));
  
  ALTER TABLE `foodOrder`.`cart` 
ADD COLUMN `userId` INT NOT NULL AFTER `isCompleted`,
ADD INDEX `userId_idx` (`userId` ASC);
ALTER TABLE `foodOrder`.`cart` 
ADD CONSTRAINT `userId`
  FOREIGN KEY (`userId`)
  REFERENCES `foodOrder`.`userDetails` (`userId`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
  
  CREATE TABLE `foodOrder`.`cartOrderMapping` (
  `cartId` INT NULL,
  `OrderId` INT NULL,
  INDEX `masertOrderId_idx` (`cartId` ASC),
  INDEX `subOrderId_idx` (`OrderId` ASC),
  );
  
  
  CREATE TABLE `foodOrder`.`addressDetails` (
  `addressId` INT NOT NULL AUTO_INCREMENT,
  `addrLine1` VARCHAR(255) NULL,
  `addrLine2` VARCHAR(255) NULL,
  `pincode` VARCHAR(45) NULL,
  `state` VARCHAR(45) NULL,
  `country` VARCHAR(45) NULL,
  `userId` INT NULL,
  PRIMARY KEY (`addressId`),
  INDEX `address_user_FK_idx` (`userId` ASC),
  CONSTRAINT `address_user_FK`
    FOREIGN KEY (`userId`)
    REFERENCES `foodOrder`.`userDetails` (`userId`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
    
ALTER TABLE `foodOrder`.`addressDetails` 
ADD COLUMN `isResturantAdd` VARCHAR(45) NULL DEFAULT 0 AFTER `userId`;


ALTER TABLE `foodOrder`.`cart` 
ADD COLUMN `addressId` INT NULL AFTER `userId`,
ADD INDEX `cart_addressId_FK_idx` (`addressId` ASC);
ALTER TABLE `foodOrder`.`cart` 
ADD CONSTRAINT `cart_addressId_FK`
  FOREIGN KEY (`addressId`)
  REFERENCES `foodOrder`.`addressDetails` (`addressId`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

   