package configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@EnableWebMvc
public class ViewResolver implements WebMvcConfigurer{
	@Bean
	public org.springframework.web.servlet.ViewResolver getViewResolver () {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        System.out.println("------ view resolver ---------");
        resolver.setPrefix("/WEB-INF/html");
        resolver.setSuffix(".html");
        return resolver;
	}
	    public void addResourceHandlers(ResourceHandlerRegistry registry) {
	    	System.out.println("-----  Setting Static files resolver -------");
	    	registry.addResourceHandler("/js/**").addResourceLocations("WEB-INF/js/");
	    	registry.addResourceHandler("/css/**").addResourceLocations("WEB-INF/css/");
	    	registry.addResourceHandler("/img/content/**").addResourceLocations("WEB-INF/img/");
	    	registry.addResourceHandler("/font/**").addResourceLocations("WEB-INF/font/");
	    	registry.addResourceHandler("/**.html").addResourceLocations("WEB-INF/html/");
	    }
	    public void configureDefaultServletHandling(
	            DefaultServletHandlerConfigurer configurer) {
	    	System.out.println("---- DefaultServletHandlerConfigurer  ---");
	        configurer.enable();
	    } 
}
