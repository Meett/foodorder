package foodOrder.Utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class Util {

	public static void setLoginId(Integer userId, HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.setAttribute("userId", userId);
		session.setAttribute("isLogin", true);
	}
	public static int getLoginId(HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();
			Boolean flag =  Boolean.valueOf(session.getAttribute("isLogin").toString());
			if(flag) {
				return Integer.parseInt((String) session.getAttribute("userId").toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

}
