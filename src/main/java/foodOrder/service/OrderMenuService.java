package foodOrder.service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import foodOrder.dao.OrderMenuDAO;
import foodOrder.models.OrderMenuDetails;

@Service
public class OrderMenuService {
	@Autowired
	OrderMenuDAO orderMenuDAO;

	public Map<String, Map<String, List<OrderMenuDetails>>> getOrderDetailsByCategory (int categoryId) {
		try {
			return orderMenuDAO.getOrderDetailsByCategory(categoryId);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return Collections.emptyMap();	
	}

	public  List<Map<String,Object>> getCategoryList() {
		try {
			return orderMenuDAO.getCategoryList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Collections.emptyList();	
	}
	public  List<Map<String,Object>> getSubCategoryList() {
		try {
			return orderMenuDAO.getSubCategoryList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Collections.emptyList();	
	}
}
