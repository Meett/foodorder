package foodOrder.service;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import foodOrder.Utils.Util;
import foodOrder.dao.UserDetailsDAO;
import foodOrder.models.Address;
import foodOrder.models.UserDetails;

@Service
public class UserDetailsService {
	@Autowired
	UserDetailsDAO userDetailsDAO;

	public boolean setUserDetails(UserDetails userDetails) {
		try {
			return userDetailsDAO.setUserDetails(userDetails);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public UserDetails getUserDetails(int userId) {
		try {
			return userDetailsDAO.getUserDetails(userId);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean loginReq(UserDetails userDetails, HttpServletRequest request) {
		try {
			return userDetailsDAO.loginReq(userDetails, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public List<Address> getAddress(int loginId, int isResturantAdd) {
		try {
			return userDetailsDAO.getAddress(loginId,isResturantAdd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Collections.emptyList();
	}
	public List<Address> getAddressFromCartId (int cartId) {
		try {
			return userDetailsDAO.getAddressFromCartId(cartId);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return Collections.emptyList();
	}
	public boolean saveAddress(Address address, int loginId) {
		try {
			if(loginId > 0 ) {
				address.setUserId(loginId);
				return userDetailsDAO.saveAddress(address);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean checkIfLogin(HttpServletRequest request) {
		try {
			Integer loginId = Util.getLoginId(request);
			return loginId > 0 ? true : false ;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
