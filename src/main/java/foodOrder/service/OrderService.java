package foodOrder.service;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import foodOrder.dao.OrderDAO;
import foodOrder.models.Cart;
import foodOrder.models.Order;

@Service
public class OrderService {
	@Autowired
	OrderDAO orderDAO;
	@Autowired 
	UserDetailsService userDetailsService;
	
	
	public List<Order> getOrderList(int userId) {
		List<Order> orderList = orderDAO.getOrderList(userId); 
		return orderList ;
	}
	@Transactional
	public boolean saveOrderList(List<Order> orderList,int userId,int isForDelivery, int addressId) {
		try {
			int orderId = orderDAO.getMaxOrderId() + 1;
			AtomicInteger atmTotalValue = new AtomicInteger();
			orderList.forEach(order -> {
				order.setOrderId(orderId);
				atmTotalValue.addAndGet((order.getPrice() * order.getQuantity()));
			});
			orderDAO.saveOrderList(orderList);
			orderDAO.saveOrderUserMapping(orderId, userId);
			int totalValue = atmTotalValue.get();
			Cart cart = new Cart(0, totalValue, "", isForDelivery, userId,addressId);
			int cartId = orderDAO.saveCartDetails(cart);
			orderDAO.saveCartOrderMapping(orderId, cartId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	public List<Cart> getAllCartList(int loginId) {
		try {
			List<Cart> cartList = orderDAO.getCartList(loginId);
			cartList.forEach(cartObj ->{
				cartObj.setOrderList(orderDAO.getOrderList(loginId, cartObj.getCartId()));
				cartObj.setAddress(userDetailsService.getAddressFromCartId(cartObj.getCartId()).get(0));
			});
			return cartList;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return Collections.emptyList();
	}
	public boolean cancelOrder(int cartId, int orderId, Cart cart) {
		if(cartId > 0) {
			try {
				Date date1=new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(cart.getCreatedDate());  
				long diff = System.currentTimeMillis() - date1.getTime();
				long diffMinutes = diff / (60 * 1000) % 60;
				if(diffMinutes <= 5) {
					orderDAO.cancelCart(cartId);
					orderDAO.cancelOrder(orderId);
					return true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}
}
