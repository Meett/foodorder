package foodOrder.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import foodOrder.models.OrderMenuDetails;
import foodOrder.service.OrderMenuService;

@RestController
public class OrderMenuController {
	@Autowired
	OrderMenuService orderMenuService;
	
	@RequestMapping(value="/getOrderMenuDetailsByCategory/{categoryId}", method= {RequestMethod.GET,RequestMethod.POST})
	public Map<String, Map<String, List<OrderMenuDetails>>> getOrderDetailsByCategory (@PathVariable("categoryId") int categoryId) {
		return orderMenuService.getOrderDetailsByCategory(categoryId);
	}
	@RequestMapping(value="/getCategoryList", method= {RequestMethod.GET,RequestMethod.POST})
	public List<Map<String,Object>> getCategoryList () {
		return orderMenuService.getCategoryList();
	}
	@RequestMapping(value="/getSubCategoryList", method= {RequestMethod.GET,RequestMethod.POST})
	public List<Map<String,Object>> getSubCategoryList() {
		return orderMenuService.getSubCategoryList();
	}
	
}
