package foodOrder.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {
	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	@RequestMapping(value="/test",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public String ActionIdOpcodeRelList() {
		System.out.println("------- test --------   " );
		//namedParameterJdbcTemplate.queryForList("select * from test ", a);
		return "test";
	}
}
