package foodOrder.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import foodOrder.Utils.Util;
import foodOrder.models.Address;
import foodOrder.models.UserDetails;
import foodOrder.service.UserDetailsService;

@RestController
public class UserDetailsController {
	@Autowired
	UserDetailsService userDetailsService;
	
	@RequestMapping(value="/setUserDetails", method=RequestMethod.POST)
	public boolean setUserDetails (@RequestBody UserDetails userDetails) {
		return userDetailsService.setUserDetails(userDetails);	
	}
	@RequestMapping(value="/getUserDetails/{userId}", method=RequestMethod.GET)
	public UserDetails getUserDetails (@PathVariable int userId) {
		return userDetailsService.getUserDetails(userId);	
	}
	@RequestMapping(value="/loginReq", method=RequestMethod.POST)
	public boolean loginReq (@RequestBody UserDetails userDetails,HttpServletRequest request) {
		return userDetailsService.loginReq(userDetails, request);	
	}
	@RequestMapping(value="/getAddress/{isResturantAdd}", method=RequestMethod.GET)
	public List<Address> getAddress (HttpServletRequest request,@PathVariable("isResturantAdd") int isResturantAdd) {
		return userDetailsService.getAddress(Util.getLoginId(request),isResturantAdd);	
	}
	@RequestMapping(value="/saveAddress", method=RequestMethod.POST)
	public boolean saveAddress (@RequestBody Address address,HttpServletRequest request) {
		return userDetailsService.saveAddress(address,Util.getLoginId(request));	
	}
	@RequestMapping(value="/checkIfLogin", method=RequestMethod.GET)
	public boolean checkIfLogin (HttpServletRequest request) {
		return userDetailsService.checkIfLogin(request);	
	}
}
