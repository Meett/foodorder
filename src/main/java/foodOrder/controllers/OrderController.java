package foodOrder.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import foodOrder.Utils.Util;
import foodOrder.models.Cart;
import foodOrder.models.Order;
import foodOrder.service.OrderService;

@RestController
public class OrderController {
	
	@Autowired
	OrderService orderService;
	
	@RequestMapping(value="/getOrderList",method=RequestMethod.GET)
	public List<Order> getOrderList(HttpServletRequest request) {
		return orderService.getOrderList(Util.getLoginId(request));
	}
	@RequestMapping(value="/saveOrderList/{isForDelivery}/{addressId}",method=RequestMethod.POST)
	public boolean saveOrderList(@RequestBody List<Order> orderList,@PathVariable("isForDelivery") int isForDelivery,@PathVariable("addressId") int addressId,HttpServletRequest request) {
		return orderService.saveOrderList(orderList,Util.getLoginId(request), isForDelivery,addressId);
	}
	@RequestMapping(value="/getAllCartList",method=RequestMethod.GET)
	public List<Cart> getAllCartList(HttpServletRequest request) {
		return orderService.getAllCartList(Util.getLoginId(request));
	}
	
	@RequestMapping(value="/cancelOrder/{cartId}/{orderId}",method=RequestMethod.POST)
	public boolean cancelOrder(@PathVariable("cartId") int cartId, @PathVariable("orderId") int orderId,@RequestBody Cart cart) {
		return orderService.cancelOrder(cartId,orderId,cart);
	}
}
