package foodOrder.models;

public class Order {
	private int OrderId;
	private int quantity;
	private String specialReq;
	private int price ;
	private String desc;
	private int id;
	private String subCategoryName;
	private String categoryName;
	private int subCategoryId;
	private int categoryId;
	private String itemName;
	private String creaedDate;
	
	 
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getOrderId() {
		return OrderId;
	}
	public void setOrderId(int orderId) {
		OrderId = orderId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getSpecialReq() {
		return specialReq;
	}
	public void setSpecialReq(String specialReq) {
		this.specialReq = specialReq;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSubCategoryName() {
		return subCategoryName;
	}
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public int getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(int subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getCreaedDate() {
		return creaedDate;
	}
	public void setCreaedDate(String creaedDate) {
		this.creaedDate = creaedDate;
	}
	@Override
	public String toString() {
		return new String("OrderId =" + OrderId
				+ " quantity =" + quantity
				+ " specialReq =" + specialReq
				+ " desc =" + desc
				+ " id =" + id
				+ " price = "+ price
				+ " subCategoryName =" + subCategoryName
				+ " categoryName =" + categoryName
				+ " subCategoryId =" + subCategoryId
				+ " categoryId =" + categoryId
				+ " itemName =" + itemName);
	}
}
