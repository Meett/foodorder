package foodOrder.models;

public class OrderUserMapping {
	private int userId;
	private int OrderId;
	private boolean isForDelivery;
	private String createdDate;
	private boolean isOrderCompleted;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getOrderId() {
		return OrderId;
	}
	public void setOrderId(int orderId) {
		OrderId = orderId;
	}
	public boolean isForDelivery() {
		return isForDelivery;
	}
	public void setForDelivery(boolean isForDelivery) {
		this.isForDelivery = isForDelivery;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public boolean isOrderCompleted() {
		return isOrderCompleted;
	}
	public void setOrderCompleted(boolean isOrderCompleted) {
		this.isOrderCompleted = isOrderCompleted;
	}
	
	
}
