package foodOrder.models;

import java.util.List;

public class Cart {
	private int cartId ;
	private int totalValue;
	private String description;
	private int isForDelivery;
	private int isCompleted;
	private int userId;
	private int addressId;
	private List<Order> orderList;
	private Address address;
	private String createdDate;
	private int orderId;
	
	
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public List<Order> getOrderList() {
		return orderList;
	}
	public void setOrderList(List<Order> orderList) {
		this.orderList = orderList;
	}
	public int getAddressId() {
		return addressId;
	}
	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}
	public Cart() {};
	public Cart(int cartId, int totalValue, String description, int isForDelivery,int userId, int addressId) {
		super();
		this.cartId = cartId;
		this.totalValue = totalValue;
		this.description = description;
		this.isForDelivery = isForDelivery;
		this.userId = userId;
		this.addressId = addressId;
	}
	public int getCartId() {
		return cartId;
	}
	public void setCartId(int cartId) {
		this.cartId = cartId;
	}
	public int getTotalValue() {
		return totalValue;
	}
	public void setTotalValue(int totalValue) {
		this.totalValue = totalValue;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getIsForDelivery() {
		return isForDelivery;
	}
	public void setIsForDelivery(int isForDelivery) {
		this.isForDelivery = isForDelivery;
	}
	public int getIsCompleted() {
		return isCompleted;
	}
	public void setIsCompleted(int isCompleted) {
		this.isCompleted = isCompleted;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	
	
	
}
