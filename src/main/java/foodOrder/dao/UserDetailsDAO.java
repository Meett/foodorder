package foodOrder.dao;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import foodOrder.Utils.Util;
import foodOrder.models.Address;
import foodOrder.models.UserDetails;

@Repository
public class UserDetailsDAO {
	@Autowired
	NamedParameterJdbcTemplate nameParameterJdbcTemplate;

	public UserDetails getUserDetails(int userId) {
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		List<UserDetails> lis = nameParameterJdbcTemplate.query("select userId,email,phone,name from userDetails where userId=:userId ", parameterSource , new BeanPropertyRowMapper<UserDetails>());
		return lis.get(0);
	}

	public boolean setUserDetails(UserDetails userDetails) {
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("email", userDetails.getEmail());
		parameterSource.addValue("pwd", userDetails.getPwd());
		parameterSource.addValue("phone", userDetails.getPhone());
		parameterSource.addValue("name", userDetails.getName());
		nameParameterJdbcTemplate.update("INSERT INTO userDetails(name,email,phone,pwd) VALUES (:name,:email,:phone,:pwd)", parameterSource);
		return true;
	}

	public boolean loginReq(UserDetails userDetails, HttpServletRequest request) {
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("email", userDetails.getEmail());
		parameterSource.addValue("pwd", userDetails.getPwd());
		Map<String,Object> map = nameParameterJdbcTemplate.queryForMap("SELECT userId FROM userDetails WHERE email=:email AND pwd=:pwd ", parameterSource);
		Integer userId = Integer.parseInt(map.get("userId").toString());
		if (userId != null && userId > 0 ) {
			Util.setLoginId(userId, request);
			return true;
		}
		return false;
	}

	public List<Address> getAddress(int loginId, int isResturantAdd) {
		StringBuilder query = new StringBuilder("SELECT addressId,addrLine1,addrLine2,pinCode,state,country FROM addressDetails ");
		if(isResturantAdd == 1)
			query.append(" WHERE isResturantAdd=:isResturantAdd ");
		else
			query.append(" WHERE userId = :userId ");
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("userId",loginId);
		parameterSource.addValue("isResturantAdd",isResturantAdd);
		return nameParameterJdbcTemplate.query(query.toString(), parameterSource,new BeanPropertyRowMapper<Address>(Address.class));
	}
	
	public List<Address> getAddressFromCartId (int cartId) {
		StringBuilder query = new StringBuilder("SELECT adds.addressId,addrLine1,addrLine2,pinCode,state,country FROM addressDetails adds");
		query.append(" INNER JOIN cart c on  c.addressId = adds.addressId and c.cartId=:cartId ");
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("cartId",cartId);
		return nameParameterJdbcTemplate.query(query.toString(), parameterSource,new BeanPropertyRowMapper<Address>(Address.class));
	}
	

	public boolean saveAddress(Address address) {

		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("addrLine1", address.getAddrLine1());
		parameterSource.addValue("addrLine2", address.getAddrLine2());
		parameterSource.addValue("pinCode", address.getPinCode());
		parameterSource.addValue("state", address.getState());
		parameterSource.addValue("country", address.getCountry());
		parameterSource.addValue("userId", address.getUserId());
		int count = nameParameterJdbcTemplate.
		update("INSERT INTO addressDetails (addrLine1,addrLine2,pinCode,state,country,userId,isResturantAdd) "
				+ "VALUES (:addrLine1,:addrLine2,:pinCode,:state,:country,:userId,0) ", parameterSource);
		return count > 0  ? true : false;
	}

}
