package foodOrder.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import foodOrder.models.OrderMenuDetails;

@Repository
@Lazy
public class OrderMenuDAO {
	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	
	/*
	 * it will return menu details by @category id value
	 * basically return format as category mapped with is sub category and sub category mapped with List<OrderMenuDetails>
	 * if category id value is 0 than it will return all menu details.
	 * */
	public Map<String, Map<String, List<OrderMenuDetails>>> getOrderDetailsByCategory(int categoryId) {
		StringBuilder query = new StringBuilder("SELECT ord.id,itemName,ord.desc,ord.categoryId,ord.subCategoryId,sub.subCategoryName,cat.categoryName,ord.price "
				+ "FROM OrderMenuDetails ord " +
				" 	INNER JOIN subCategoryDetails sub on ord.subCategoryId=sub.id " + 
				"   INNER JOIN categoryDetails cat on ord.categoryId = cat.id ");
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		if(categoryId > 0) {
			query.append("WHERE categoryId = :categoryId ");
			parameterSource.addValue("categoryId", categoryId);
		}
		query.append(" GROUP BY ord.categoryId , ord.subCategoryId ,ord.id");
		List<OrderMenuDetails>  menuOrderList = 
				namedParameterJdbcTemplate.query(query.toString(), parameterSource,new BeanPropertyRowMapper<OrderMenuDetails>(OrderMenuDetails.class) );
		Map<String,Map<String,List<OrderMenuDetails>>> returnMap = new HashMap<>();
		List<OrderMenuDetails> menuList = Collections.emptyList();
		Map<String,List<OrderMenuDetails>> subCategoryMap = Collections.emptyMap();
		for (OrderMenuDetails orderMenuDetails : menuOrderList) {
			if(returnMap.containsKey(orderMenuDetails.getCategoryName())) {
				subCategoryMap = returnMap.get(orderMenuDetails.getCategoryName());
				if(subCategoryMap.containsKey(orderMenuDetails.getSubCategoryName())) {
					menuList =  subCategoryMap.get(orderMenuDetails.getSubCategoryName());
					menuList.add(orderMenuDetails);
				}else {
					menuList = new ArrayList<>();
					menuList.add(orderMenuDetails);
					subCategoryMap.put(orderMenuDetails.getSubCategoryName(), menuList);
				}
			}else {
				subCategoryMap = new HashMap<>();
				menuList = new ArrayList<>();
				menuList.add(orderMenuDetails);
				subCategoryMap.put(orderMenuDetails.getSubCategoryName(), menuList);
				returnMap.put(orderMenuDetails.getCategoryName(), subCategoryMap);
			}
				
		}
		
		return returnMap; 
		
	}
	public List<Map<String,Object>> getCategoryList(){
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		return namedParameterJdbcTemplate.queryForList("SELECT id,categoryName FROM categorydetails", parameterSource);
	}
	public List<Map<String,Object>> getSubCategoryList(){
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		return namedParameterJdbcTemplate.queryForList("SELECT id,subcategoryName FROM subcategorydetails;", parameterSource);
	}

}
