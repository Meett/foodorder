package foodOrder.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import foodOrder.models.Cart;
import foodOrder.models.Order;

@Repository
public class OrderDAO {
	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	public List<Order> getOrderList(int userId){
		return getOrderList(userId,0);
	}
	public List<Order> getOrderList(int userId,int cartId) {
		StringBuilder query = new StringBuilder("select ord.OrderId,ord.quantity,ord.specialReq,orMenu.price as price,orMenu.itemName," +
				" orMenu.desc,orMenu.id," + 
				"	sub.subCategoryName,cat.categoryName,sub.id as subCategoryId,cat.id as categoryId" + 
				"  FROM orderDetails ord" + 
				"	 INNER JOIN OrderUserMapping  orUsr ON  orUsr.OrderId = ord.OrderId and userId = :userId " + 
				"    INNER JOIN OrderMenuDetails orMenu ON orMenu.id = ord.orderMenuId" + 
				"    INNER JOIN subCategoryDetails sub ON orMenu.subCategoryId=sub.id" + 
				"    INNER JOIN categoryDetails cat ON orMenu.categoryId = cat.id ");
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		if(cartId > 0) {
			query.append(" INNER JOIN cartOrderMapping cmap ON ord.OrderId = cmap.OrderId and cmap.cartId = :cartId ");
			parameterSource.addValue("cartId", cartId);
		}
		parameterSource.addValue("userId",userId);
		List<Order> orderList = namedParameterJdbcTemplate.query(query.toString(),parameterSource,new BeanPropertyRowMapper<Order>(Order.class));
		Map<Integer,List<Map<String, Object>>> returnMap = new HashMap<>();
		List<Map<String, Object>> list = Collections.emptyList();
		for (Order order : orderList) {
			Map<String,Object> orderMap = new HashMap<>();
			orderMap.put("categoryId", order.getCategoryId());
			orderMap.put("categoryName", order.getCategoryName());
			orderMap.put("id", order.getId());
			orderMap.put("quantity", order.getQuantity());
			orderMap.put("specialReq", order.getSpecialReq());
			orderMap.put("subCategoryId", order.getSubCategoryId());
			orderMap.put("subCategoryName", order.getSubCategoryName());
			orderMap.put("price", order.getPrice());
			orderMap.put("itemName", order.getItemName());
			if (returnMap.containsKey(order.getOrderId())) 
				list = returnMap.get(order.getOrderId());
			else 
				list = new ArrayList<>();
			
			list.add(orderMap);
			returnMap.put(order.getOrderId(), list);
		}
		return orderList;
	}
	public boolean saveOrderList(List<Order> orderList) {
		StringBuilder  query = new StringBuilder("INSERT INTO orderDetails (orderId,orderMenuId,specialReq,quantity,price) "
				+" VALUES (:orderId,:Id,:specialReq,:quantity,:price)");
		SqlParameterSource[] batchValues = SqlParameterSourceUtils.createBatch(orderList);
		namedParameterJdbcTemplate.batchUpdate(query.toString(), batchValues);
		return false;
	}
	public boolean saveOrderUserMapping(int orderId, int userId) {
		StringBuilder query = new StringBuilder(""
				+ "INSERT INTO orderUserMapping (OrderId,userId,createdDate) VALUES (:orderId,:userId,(SELECT CURRENT_TIMESTAMP()))");
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("userId",userId);
		parameterSource.addValue("orderId",orderId);
		namedParameterJdbcTemplate.update(query.toString(), parameterSource);
		return false;
	}
	public int saveCartDetails(Cart cart) {
		StringBuilder query = new StringBuilder(""
				+ "INSERT INTO cart (totalValue,description,isForDelivery,userId,addressId) VALUES (:totalValue,:description,:isForDelivery,:userId,:addressId)");
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("totalValue",cart.getTotalValue());
		parameterSource.addValue("description",cart.getDescription());
		parameterSource.addValue("isForDelivery",cart.getIsForDelivery());
		parameterSource.addValue("userId",cart.getUserId());
		parameterSource.addValue("addressId",cart.getAddressId());
		KeyHolder holder = new GeneratedKeyHolder();
		namedParameterJdbcTemplate.update(query.toString(), parameterSource,holder);
		return holder.getKey().intValue();
	}
	public List<Cart> getCartList(int userId) {
		StringBuilder query = new StringBuilder (" SELECT c.cartId,totalValue,description,isForDelivery,userId,addressId,isCompleted,createdDate,cartOr.OrderId as orderId"
				+ " from cart c"
				+ " INNER JOIN cartOrderMapping cartOr ON cartOr.cartId = c.cartId where userId = :userId ");
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("userId",userId);
		return namedParameterJdbcTemplate.query(query.toString(), parameterSource,new BeanPropertyRowMapper<Cart>(Cart.class));
	}
	
	
	public boolean saveCartOrderMapping (int orderId,int cartId) {
		StringBuilder query = new StringBuilder(""
				+ "INSERT INTO cartOrderMapping (cartId,OrderId) VALUES (:cartId,:orderId)");
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("orderId",orderId);
		parameterSource.addValue("cartId",cartId);
		namedParameterJdbcTemplate.update(query.toString(), parameterSource);
		return true;
	}
	public int getMaxOrderId() {
		StringBuilder query = new StringBuilder("SELECT CASE WHEN MAX(orderId) is null THEN 0 ELSE MAX(orderId) END as orderId FROM orderDetails");
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		return namedParameterJdbcTemplate.queryForObject(query.toString(), parameterSource,Integer.class);
	}
	public boolean cancelOrder(int orderId) {
		StringBuilder query = new StringBuilder("delete from orderDetails where OrderId = :OrderId;");
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("OrderId",orderId);
		namedParameterJdbcTemplate.update(query.toString(), parameterSource);
		return true;
	}
	public boolean cancelCart(int cartId) {
		StringBuilder query = new StringBuilder("delete from cart where cartId = :cartId ;  ");
		MapSqlParameterSource parameterSource = new MapSqlParameterSource();
		parameterSource.addValue("cartId",cartId);
		namedParameterJdbcTemplate.update(query.toString(), parameterSource);
		query.setLength(0);
		query.append("DELETE FROM cartOrderMapping where cartId = :cartId ");
		namedParameterJdbcTemplate.update(query.toString(), parameterSource);
		return true;
	}

}
