var myapp =angular.module('foodOrderApp',['oc.lazyLoad','foodOrder.service','ui.bootstrap']);
(function(myapp){
'use strict';
myapp.controller('MainController',['$http','$ocLazyLoad',MainController]);
function MainController($http,$ocLazyLoad){
    var main = this;
    main.init = init;
    main.cssJsFiles = [
        'css/bootstrap.css',
        'css/font-awesome.min.css',
        'css/masterslider.css',
        'css/masterSilderstyle.css',
        'css/style.css',
        'css/responsive.css',
        'css/owl.theme.css',
        'css/owl.carousel.css',
        'css/w3.css',
       
        'js/masterslider.min.js',
        'js/jquery-ui-1.10.4.custom.min.js',
        'js/owl.carousel.js',
        'js/moment.js',
        'js/jquery.ui.map.js',
        'js/jquery.magnific-popup.min.js'];

    function init(){

        // $ocLazyLoad.load([
        //     { 
        //         files: [
        //             'js/ui-bootstrap.js',
        //             'js/bootstrap.js',
        //             'js/home/foodOrderService.js',
        //             'js/home/homeController.js',
        //     ],
        //         cache: false
        //     }
        // ]).then(function(){
            $ocLazyLoad.load([
                { 
                    files: main.cssJsFiles,
                    cache: false
                }
                ]).then(function(){
                    main.url = 'home.html';
                });
        // });
    }
    init();
}

})(myapp);