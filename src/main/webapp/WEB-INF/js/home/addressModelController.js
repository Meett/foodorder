(function(myapp){
    'use strict';
    //angular.module('foodOrder.address',['oc.lazyLoad','ui.bootstrap'])
    myapp.controller('addressModelController',['$ocLazyLoad','foodOrderService','$uibModalInstance','data',addressModelController]);
    function addressModelController($ocLazyLoad,foodOrderService,$uibModalInstance,data){
        var addCtrl = this;
        addCtrl.isLogin = false;
        addCtrl.isForDelivery = 0;
        addCtrl.isConfirm =false;
        addCtrl.totalPrice = data.totalPrice;

        addCtrl.saveCartDetails=data.saveCartDetails;
        addCtrl.addOrRemoveItem=addOrRemoveItem;
        addCtrl.addAddress =addAddress;
        addCtrl.init=init;
        addCtrl.selectAddress=selectAddress;
        addCtrl.onChangeRadio=onChangeRadio;
        addCtrl.confirmOrder=confirmOrder;
        addCtrl.checkout=checkout;

        function init(){
            checkIfLogin();
        }
        function checkIfLogin() {
            foodOrderService.checkIfLogin()
                .then(successCallBack, errorCallBack);
            function successCallBack(response) {
                if (!angular.isUndefined(response) && response.data === true){
                    addCtrl.isLogin = true;
                    getUserAddress();
                    getRestaurantAddress();
                }
            }
            function errorCallBack() {
                alert('Something went wrong');
            }
        }
        addCtrl.ok = function (data) {
            $uibModalInstance.close(data);
          };
      
        addCtrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        function checkout(){
            var json = {
                saveCartDetails :   addCtrl.saveCartDetails,
                addressId  :   addCtrl.selectedAddress.addressId,
                isForDelivery   :   addCtrl.isForDelivery
            };
            addCtrl.ok(json);
        }
        function getUserAddress(){
            foodOrderService.getUserAddress(0)
                .then(successCallBack,errorCallBack);
            function successCallBack(response){
                addCtrl.userAddressList = response.data;
            }
            function errorCallBack(){
                alert('Something went wrong');
            }
        }
        function getRestaurantAddress(){
            foodOrderService.getUserAddress(1)
                .then(successCallBack,errorCallBack);
            function successCallBack(response){
                addCtrl.resAddressList = response.data;
                onChangeRadio();
            }
            function errorCallBack(){
                alert('Something went wrong');
            }
        }
        function onChangeRadio (){
            if(addCtrl.isForDelivery == 1)
                addCtrl.addressList = angular.copy(addCtrl.userAddressList);
            else
                addCtrl.addressList = angular.copy(addCtrl.resAddressList);
        }


        function addAddress(){
            foodOrderService.saveUserAddress(addCtrl.adressDetails)
                .then(successCallBack,errorCallBack);
            function successCallBack(response){
                addCtrl.userAddressList = response;
            }
            function errorCallBack(){
                alert('Something went wrong');
            }
        }
        function selectAddress(address){
            addCtrl.selectedAddress = address;
            console.log(addCtrl);
        }

        function addOrRemoveItem(item){
            var index = addCtrl.saveCartDetails.indexOf(item);
            if(index > -1 && item.quantity == 0)
                addCtrl.saveCartDetails.splice(index, 1);
            else if (index > -1 && item.quantity > 0)
                addCtrl.saveCartDetails[index] = item;
            else if(index == -1 && item.quantity > 0)
                addCtrl.saveCartDetails.push(item);
            else if (index > -1 )
                addCtrl.saveCartDetails.splice(index, 1);
            calculateTotalPrice();
        }
        function calculateTotalPrice(){
            addCtrl.totalPrice = 0 ;
            addCtrl.saveCartDetails.forEach(element => {
                addCtrl.totalPrice += element.price * element.quantity;
           });
        }
        function confirmOrder(){
            if(angular.isDefined(addCtrl.selectedAddress))
                addCtrl.isConfirm=true;
        }
        init();
    }
    
    })(myapp);