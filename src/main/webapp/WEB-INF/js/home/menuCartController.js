(function(myapp){
    'use strict';
    //angular.module('foodOrder.menu',['oc.lazyLoad','ui.bootstrap'])
    myapp.controller('menuCartController',['$http','$ocLazyLoad','foodOrderService','$uibModal',menuCartController]);
    function menuCartController($http,$ocLazyLoad,foodOrderService,$uibModal){
        var menuCtrl = this;
        menuCtrl.init = init;
        menuCtrl.submit=submit;
        menuCtrl.addOrRemoveItem =addOrRemoveItem;
        menuCtrl.getCartDetails = getCartDetails;
        menuCtrl.openUserDetailsModel=openUserDetailsModel;


        menuCtrl.saveCartDetails=[];
        menuCtrl.isForDelivery = 0;
        function init(){
            var categoryId = 0;
            getCartDetails(categoryId);
            getCategoryList();
        }
        function getCartDetails(categoryId){
            foodOrderService.getOrderMenuDetailsByCategory(categoryId)
                .then(successCallBack,errorCallBack);
            function successCallBack(response){
                menuCtrl.cartDetails = response.data;
            }
            function errorCallBack(){
                alert('Something went wrong');
            }
        }

        function getCategoryList(){
            foodOrderService.getCategoryList()
                .then(successCallBack,errorCallBack);
            function successCallBack(response){
                menuCtrl.categoryList = response.data;
            }
            function errorCallBack(){
                alert('Something went wrong');
            }    
        }


        function submit(){
            console.log(menuCtrl.saveCartDetails);
            foodOrderService.saveOrderList(menuCtrl.saveCartDetails,menuCtrl.isForDelivery,menuCtrl.addressId)
            .then(successCallBack,errorCallBack);
            function successCallBack(response){
                alert('Order Plcae succes fully');
                foodOrderService.openOrderReviewPage();
            }
            function errorCallBack(){
                alert('Something went wrong');
            }
        }
        function addOrRemoveItem(item){
            var index = menuCtrl.saveCartDetails.indexOf(item);
            if(index > -1 && item.quantity == 0)
                menuCtrl.saveCartDetails.splice(index, 1);
            else if (index > -1 && item.quantity > 0)
                menuCtrl.saveCartDetails[index] = item;
            else if(index == -1 && item.quantity > 0)
                menuCtrl.saveCartDetails.push(item);
            else if (index > -1 )
                menuCtrl.saveCartDetails.splice(index, 1);
            calculateTotalPrice();
        }
        function calculateTotalPrice(){
            menuCtrl.totalPrice = 0 ;
            menuCtrl.saveCartDetails.forEach(element => {
                menuCtrl.totalPrice += element.price * element.quantity;
           });
        }
        function openUserDetailsModel(){
            // var modalInstance = $uibModal.open({
            //     animation: true,
            //     ariaLabelledBy: 'modal-title',
            //     ariaDescribedBy: 'modal-body',
            //     templateUrl: '/userModel.html',
            //     controller: 'userModelController',
            //     controllerAs: 'uCtrl',
            //     size: 'lg',
            //     resolve: {
            //         saveCartDetails: function () {
            //             return menuCtrl.saveCartDetails;
            //         }
            //     }
            //   });

            //   modalInstance.result.then(function (userId) {
            //     menuCtrl.userId = userId;
            //   }, function () {
            //     console.log('Modal dismissed at: ' + new Date());
            //   });
            foodOrderService.operUserModel(menuCtrl.saveCartDetails,afterUserModel);
        }
        function afterUserModel(data){
            debugger;
            console.log("userModel close");
            if (data === true)
                openAddressModel();
        }
        function openAddressModel(){
            var json = {
                saveCartDetails : menuCtrl.saveCartDetails,
                addOrRemoveItem : menuCtrl.addOrRemoveItem,
                totalPrice      : menuCtrl.totalPrice
            }
            foodOrderService.openAddressModel(json,afterAddressModel);
        }
        function afterAddressModel(data){
            debugger;
            console.log("afterAddressModel close");
            console.log(data);
            if(angular.isDefined(data.saveCartDetails)){
                menuCtrl.saveCartDetails = data.saveCartDetails ;
                menuCtrl.isForDelivery = data.isForDelivery;
                menuCtrl.addressId = data.addressId;
                calculateTotalPrice();
                submit();
            }
        }
        init();
    }
    
})(myapp);