(function(myapp){
    'use strict';
    //angular.module('foodOrder.user',['oc.lazyLoad','ui.bootstrap'])
    myapp.controller('userModelController',['$ocLazyLoad','foodOrderService','$uibModalInstance','saveCartDetails',userModelController]);
    function userModelController($ocLazyLoad,foodOrderService,$uibModalInstance,saveCartDetails){
        var uCtrl = this;
        uCtrl.isLogin = false;
        uCtrl.isForDelivery = 0;

        uCtrl.saveUserDetails = saveUserDetails;
        uCtrl.login = login;
        uCtrl.init=init;

        function init(){
            checkIfLogin();
        }
        function checkIfLogin() {
            foodOrderService.checkIfLogin()
                .then(successCallBack, errorCallBack);
            function successCallBack(response) {
                if (!angular.isUndefined(response) && response.data === true){
                    uCtrl.isLogin = true;
                    uCtrl.ok();
                }
            }
            function errorCallBack() {
                alert('Something went wrong');
            }
        }
        uCtrl.ok = function () {
            $uibModalInstance.close(uCtrl.isLogin);
          };
      
        uCtrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        function login(){
            foodOrderService.loginReq(uCtrl.loginUser)
                .then(successCallBack,errorCallBack);
            function successCallBack(response){
                if(!angular.isUndefined(response) && response.data === true){
                    uCtrl.isLogin = true;
                    uCtrl.ok();
                }
            }
            function errorCallBack(){
                alert('Something went wrong');
            }

        }
        function saveUserDetails(){
            foodOrderService.setUserDetails(uCtrl.userDetails)
                .then(successCallBack,errorCallBack);
            function successCallBack(response){
                if(!angular.isUndefined(response) && response.data === true)
                    uCtrl.isLogin = true;
                    getUserAddress();
            }
            function errorCallBack(){
                alert('Something went wrong');
            }

        
        }
        function getUserAddress(){
            foodOrderService.getUserAddress()
                .then(successCallBack,errorCallBack);
            function successCallBack(response){
                debugger;
                uCtrl.userAddressList = response.data;
            }
            function errorCallBack(){
                alert('Something went wrong');
            }
        }
        init();
    }
    
    })(myapp);