(function(myapp){
    'use strict';
    //angular.module('foodOrder.menu',['oc.lazyLoad','ui.bootstrap'])
    myapp.controller('orderReviewController',['$http','$ocLazyLoad','foodOrderService','$uibModal',orderReviewController]);
    function orderReviewController($http,$ocLazyLoad,foodOrderService,$uibModal){
        var orCtrl = this;

        orCtrl.checkIfLogin =checkIfLogin;
        orCtrl.h5click=h5click;
        orCtrl.checkofCancel=checkofCancel;
        orCtrl.cancelOrder=cancelOrder;


        function init(){
            checkIfLogin();
        }
        function checkIfLogin() {
            foodOrderService.checkIfLogin()
                .then(successCallBack, errorCallBack);
            function successCallBack(response) {
                if (!angular.isUndefined(response) && response.data === true){
                    orCtrl.isLogin = true;
                    getOrderDetails();
                }else{
                    foodOrderService.operUserModel({},afterUserModel);
                }
            }
            function errorCallBack() {
                alert('Something went wrong');
            }
        }
        function afterUserModel(isLogin){
            if(isLogin === true)
                getOrderDetails();
            else
                foodOrderService.operUserModel({},afterUserModel);
        }
        function getOrderDetails() {
            foodOrderService.getAllCartList()
                .then(successCallBack, errorCallBack);
            function successCallBack(response) {
                if (!angular.isUndefined(response)) {
                    orCtrl.cartList = response.data;
                    angular.forEach(orCtrl.cartList, function(cartObj) {
                        cartObj.createdDate = moment(cartObj.createdDate).format('MM/DD/YYYY HH:mm');
                        //new Date(cartObj.createdDate);
                      });
                }
            }
            function errorCallBack() {
                alert('Something went wrong');
            }
        }
        function checkofCancel(date){
            date = moment(date);
            if (moment().diff(date,"minutes") < 5)
                return true;
            return false; 
        }
        function cancelOrder(cartObj){
            if(checkofCancel(cartObj.createdDate)){
                foodOrderService.cancelOrder(cartObj.cartId, cartObj.orderId,cartObj)
                .then(successCallBack, errorCallBack);
            }
            function successCallBack(response) {
                getOrderDetails();
            }
            function errorCallBack() {
                alert('Something went wrong');
            }

        }

        function h5click(){
            $('#toggleTitle').toggleClass('active')
			.next().slideToggle();
        }


        init();
}
    })(myapp);