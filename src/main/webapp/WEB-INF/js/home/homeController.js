(function(app){
    'use strict';
   // angular.module('foodOrder.home',['oc.lazyLoad','foodOrder.service','ui.bootstrap'])
   app.controller('homeController',['$http','$ocLazyLoad','$timeout','foodOrderService',homeController]);
    function homeController($http,$ocLazyLoad,$timeout,foodOrderService){
        var hCtrl = this;
        hCtrl.isChildPage = false;
        hCtrl.init = init;
        hCtrl.openOrderPage = openOrderPage;
        hCtrl.openOrderReviewPage = openOrderReviewPage;
        foodOrderService.setOpenOrderReviewPage(hCtrl.openOrderReviewPage);
        
        $timeout(function(){  					//Because after all div initialize 
        	sliderFun();
			},100);
    
        function init(){
        }
        function openOrderPage(){
            $ocLazyLoad.load([
                {
                    files: [
                        'js/home/menuCartController.js',
                        'js/home/userModelController.js',
                        'js/home/addressModelController.js'
                    ],
                    cache: true
                }
            ]).then(function(){
                hCtrl.homePageUrl = '/menuCart.html';
                hCtrl.isChildPage = true;
            }
            );
        }
        function openOrderReviewPage(){
            $ocLazyLoad.load([
                {
                    files: [
                        'js/home/orderReviewController.js',
                    ],
                    cache: true
                }
            ]).then(function(){
                hCtrl.homePageUrl = '/orderReview.html';
                hCtrl.isChildPage = true;
            }
            );
        }
        
        function sliderFun(){
        	$ocLazyLoad.load([
                {
                    files: ['js/scripts.js'],
                    cache: false
                }
            ]).then(function(){
                debugger;
                hCtrl.slideIndex = 1;
                
                automatic();
                setInterval(automatic, 2000); 
            });
        }
        hCtrl.plusDivs =plusDivs;
        hCtrl.showDivs =showDivs;
        function automatic (){
            debugger;
            showDivs(hCtrl.slideIndex);
            hCtrl.slideIndex++;
            if(hCtrl.slideIndex > 3)
                hCtrl.slideIndex = 1;
        }


        function plusDivs(n) {
            showDivs(hCtrl.slideIndex += n);
            }

            function showDivs(n) {
            var i;
            var x = document.getElementsByClassName("mySlides");
            if (n > x.length) {hCtrl.slideIndex = 1}    
            if (n < 1) {hCtrl.slideIndex = x.length}
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";  
            }
            x[hCtrl.slideIndex-1].style.display = "block";  
        }
    }
    })(myapp);