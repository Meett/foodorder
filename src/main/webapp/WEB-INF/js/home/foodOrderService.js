(function(){
    'use strict';
    angular.module('foodOrder.service',['oc.lazyLoad','ui.bootstrap'])
    .service('foodOrderService',['$http','$ocLazyLoad','$uibModal',foodOrderService]);
    function foodOrderService($http,$ocLazyLoad,$uibModal){
        var orderReviewPageFunction;
        return {
            getOrderMenuDetailsByCategory:getOrderMenuDetailsByCategory,
            saveOrderList:saveOrderList,
            getCategoryList:getCategoryList,
            getSubCategoryList:getSubCategoryList,
            setUserDetails:setUserDetails,
            loginReq:loginReq,
            getUserAddress:getUserAddress,
            saveUserAddress:saveUserAddress,
            checkIfLogin:checkIfLogin,
            operUserModel:operUserModel,
            openAddressModel:openAddressModel,
            openOrderReviewPage:openOrderReviewPage,
            setOpenOrderReviewPage:setOpenOrderReviewPage,
            getAllCartList:getAllCartList,
            cancelOrder:cancelOrder
        };
        function httpPost(url,data){
            return $http({
                method: 'POST',
                url: url,
                data:data
            });
        }
        function httpGet(url){
            return $http({
                method: 'GET',
                url: url,
            });
        }
        function getOrderMenuDetailsByCategory(categoryId){
            var url = '/getOrderMenuDetailsByCategory/'+categoryId;
            return httpGet(url);
        }
        function saveOrderList (data,isForDelivery,addressId){
            var url = 'saveOrderList/'+isForDelivery+'/'+addressId;
            return httpPost(url,data);
        }
        function getCategoryList(){
            var url = '/getCategoryList';
            return httpGet(url);
        }
        function getSubCategoryList(){
            var url = '/getSubCategoryList';
            return httpGet(url);
        }
        function setUserDetails(data){
            var url = '/setUserDetails';
            return httpPost(url,data);
        }
        function loginReq(data){
            var url = '/loginReq';
            return httpPost(url,data);
        }
        function getUserAddress(isForDelivery){
            var url = '/getAddress/'+isForDelivery;
            return httpGet(url);
        }
        function getAllCartList(){
            var url = '/getAllCartList/';
            return httpGet(url);
        }
        function cancelOrder(cartId, OrderId, cartObj){
            var url = '/cancelOrder/'+cartId+'/'+OrderId;
            return httpPost(url,cartObj);
        }

        function saveUserAddress(data){
            var url = '/saveAddress';
            return httpPost(url,data);
        }
        function checkIfLogin(){
            var url = '/checkIfLogin';
            return httpGet(url);
        }
        function openOrderReviewPage(){
            orderReviewPageFunction();
        }
        function setOpenOrderReviewPage(functionName){
            this.orderReviewPageFunction = functionName;
        }
        function operUserModel(data,functionName){
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/userModel.html',
                controller: 'userModelController',
                controllerAs: 'uCtrl',
                size: 'lg',
                resolve: {
                    saveCartDetails: function () {
                        return data;
                    }
                }
              });
              modalInstance.result.then(function (returnData) {
                functionName(returnData);
              }, function () {
                console.log('Modal dismissed at: ' + new Date());
              });
        }

        function openAddressModel(data,functionName){
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/addressModel.html',
                controller: 'addressModelController',
                controllerAs: 'addCtrl',
                size: 'lg',
                resolve: {
                    data: function () {
                        return data;
                    }
                }
              });
              modalInstance.result.then(function (returnData) {
                functionName(returnData);
              }, function () {
                console.log('Modal dismissed at: ' + new Date());
              });
        }



    }
    
    })();